$(document).ready( function() {
	// setTimeout(function(){
	// 	$('.preloader').fadeOut( "300", function(){});
	// },3000);
	/*-------------------- MENU --------------------*/
	$('.nav-btn').on('click', function(){
		$('.nav-menu').toggle('slow');
		$(this).toggleClass('active');
		return false;
	});
	$('.dropdown').on('click', function(){
		$(this).toggleClass('active');
		$(this).children('ul').toggle('slow');
		return false;
	});
	$('.dropdown2').on('click', function(){
		$(this).toggleClass('active');
		$(this).children('ul').toggle('slow');
		return false;
	});
	/*-------------------- MASK --------------------*/
	$('.tel').mask("+7 (999) 999-99-99");
	/*-------------------- MODAL WINDOW	--------------------*/
	$('.popup-open').on('click', function(){
		$('.overlay').addClass('active');
		$('body').addClass('modal');
		$('.popup').removeClass('active');
		rel=$(this).attr('rel');
		var tN = $(this).parents('.teacher-desc').children('.hid-teacher-name').val();
		console.log(tN);
		$('.popup-'+rel).addClass('active');
		if(rel == 'action2'){
			$('.popup-name').text(tN);
		}
		return false;
	});
	$('.popup-close, .overlay').on('click', function(){
		$('.overlay').removeClass('active');
		$('body').removeClass('modal');
		$('.popup').removeClass('active');
	});
	/*----------------------REWIEVS----------------------*/
	$('.rewiev-tab').on('click', function(){
		var rewText = $(this).children('.pupil-rewiev').detach();
		var bigRewBlock = $('.single-rewiev-wrap').children('.pupil-rewiev').detach();
		rewText.appendTo('.single-rewiev-wrap');
		bigRewBlock.appendTo(this);
		var photoR = $(this).css('background-image');
		var bigPhotoR = $('.pupil-photo').css('background-image');
		$(this).css({'background-image' : bigPhotoR});
		$('.pupil-photo').css({'background-image' : photoR});
	});
	/*---------------------FULLPAGE---------------------*/
	$(window).on('load resize',function(){
		var ww = $(this).width();
		if(ww > 767){
			$('#fullpage').fullpage({
				navigation: true,
				navigationPosition: 'left',
				navigationTooltips: ['01', '02', '03', '04'],
				autoScrolling:true,
				animateAnchor: false,
				scrollingSpeed: 1000,
				anchors:['sect1', 'sect2', 'sect3', 'sect4']
			});
		}
		if( ww < 481){
			$('.rewievs-tabs').slick({
				slidesToShow: 3,
				dots: false,
				infinite: true,
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 2000,
				arrows: false
			});
			var phoneDet = $('.header-phone').detach();
			phoneDet.appendTo('.nav-menu');
		}
	});
	/*---------------------------SLIDER---------------------------*/
	$('.teacher-slider').slick({
		infinite: true,
		fade: true,
		speed: 1200,
		nextArrow: '<span class="slick-arrow-next">Следующий педагог<span class="decor-slide"></span></span>'
	});
	$('.close_vid').click(function() {
		$("iframe")[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
	});
	$('.play-one-10').click(function() {
		$(".popup-action10 iframe")[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
	});

});

